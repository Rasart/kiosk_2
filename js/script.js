/* Чекбокс */

jQuery(document).ready(function(){

jQuery(".cb").mousedown(
/* при клике на чекбоксе меняем его вид и значение */
function() {

     changeCheck(jQuery(this));
     
});


jQuery(".cb").each(
/* при загрузке страницы нужно проверить какое значение имеет чекбокс и в соответствии с ним выставить вид */
function() {
     
     changeCheckStart(jQuery(this));
     
});

                                        });

function changeCheck(el)
/* 
    функция смены вида и значения чекбокса
    el - span контейнер дял обычного чекбокса
	input - чекбокс
*/
{
     var el = el,
          input = el.find("input").eq(0);
   	 if(!input.attr("checked")) {
		el.css("background-position","0 -24px");	
		input.attr("checked", true)
	} else {
		el.css("background-position","0 0");	
		input.attr("checked", false)
	}
     return true;
}

function changeCheckStart(el)
/* 
	если установлен атрибут checked, меняем вид чекбокса
*/
{
var el = el,
		input = el.find("input").eq(0);
      if(input.attr("checked")) {
		el.css("background-position","0 -16px");	
		}
     return true;
}

		
        
/* Выпадающий список */

$(document).ready(function() {
  
  $('.sparkbox-custom').sbCustomSelect({
    appendTo: 'body'
  });
  
});

/* Переключатели */

(function($) {
$(function() {

  $('ul.tabs').delegate('li:not(.current)', 'click', function() {
    $(this).addClass('current').siblings().removeClass('current')
      .parents('body').find('div.box').hide().eq($(this).index()).fadeIn(150);
  })

})
})(jQuery)

/* Скрол */

$(function()
{
    $('.scroll-pane').jScrollPane(
        {
            horizontalGutter: 835,
		}
	);
});


/* Спойлер */

    jQuery(document).ready(function(){
        // Скрываем все спойлеры
        jQuery('.spoiler-body').hide()
        // по клику отключаем класс folded, включаем unfolded, затем для следующего
        // элемента после блока .spoiler-head (т.е. .spoiler-body) показываем текст спойлера
        jQuery('.spoiler-head').click(function(){
            jQuery(this).toggleClass("folded").toggleClass("unfolded").next().slideToggle()
        })
    })
